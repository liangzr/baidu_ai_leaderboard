const { n, nDiff } = require('./tools');

exports.printDingtalk = msg => (
  msg.newUser
    ? `
### 🎉 ${n(msg.item.score)} ${msg.subtitle}

欢迎新玩家～～

分数:  ${n(msg.item.score)}

姓名:   ${msg.item.userName}

排名:   ${msg.item.rank}
  `
    : `
### 🎯 ${n(msg.item.score)} ${msg.subtitle}

上次:   ${n(msg.old.score)}（${n(msg.item.score - msg.old.score)}）

姓名:   ${msg.item.userName}

排名:   ${msg.item.rank} （${nDiff(msg.old.rank - msg.item.rank, 0)}）
  `
);

exports.printWechat = msg => (
  msg.newUser
    ? `
## ${n(msg.item.score)} ${msg.subtitle}
欢迎新玩家～～
> 分数:   ${n(msg.item.score)}   
> 姓名:   ${msg.item.userName}   
> 排名:   ${msg.item.rank}   
  `
    : `
## ${n(msg.item.score)} ${msg.subtitle}
上次:   ${n(msg.old.score)}（${n(msg.item.score - msg.old.score)}）  
姓名:   ${msg.item.userName}   
排名:   ${msg.item.rank} （${nDiff(msg.old.rank - msg.item.rank, 0)}）   
  `
);

