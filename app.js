const http = require('http');
const schedule = require('node-schedule');
const axios = require('axios');
const querystring = require('querystring');

const {
  readPreviousData,
  updateData,
  nDiff,
  dateToString,
  pickUpUser,
} = require('./tools');
const { api, push, user } = require('./config');
const { printDingtalk, printWechat } = require('./print');

let firstBooted = true;

const sendWechatMessage = msgs => axios.post(push.pushbear.url, querystring.stringify({
  sendkey: push.pushbear.sendkey,
  text: '排名更新啦',
  desp: `
${
  msgs.holyShit
    ? '![](https://ws2.sinaimg.cn/large/006tNbRwly1fyi0tf0hc7j30sj0g848u.jpg)'
    : '![](https://ai-studio-static-default.cdn.bcebos.com/icon/match-5.png)'
}
${msgs.map(msg => printWechat(msg)).join('')}    
  `,
})).then((res) => {
  console.log(res.data);
});

const sendDingtalkMessage = msgs => axios.post(push.wolfBotHook, {
  actionCard: {
    title: msgs.holyShit ? 'Holy Shit' : '排名更新啦',
    text: `
${
  msgs.holyShit
    ? '![](https://ws2.sinaimg.cn/large/006tNbRwly1fyi0tf0hc7j30sj0g848u.jpg)'
    : '![](https://ai-studio-static-default.cdn.bcebos.com/icon/match-5.png)'
}
${msgs.map(msg => printDingtalk(msg)).join('')}
    `,
    hideAvatar: '1',
    btnOrientation: '0',
    singleTitle: '今日老黄历',
    singleURL: 'https://www.widuu.com/date/',
  },
  msgtype: 'actionCard',
}).then((res) => {
  console.log(res.data);
});


schedule.scheduleJob('*/1 * * * *', () => {
  const previousData = readPreviousData();
  axios
    .post(api.url, api.data, {
      headers: {
        Cookie: api.cookie,
      },
    })
    .then(({ data: body }) => {
      const { result: { data } } = body;
      if (JSON.stringify(data) === JSON.stringify(previousData)) {
        throw new Error('no change');
      }
      return data;
    })
    .then(data => updateData(data))
    .then((data) => {
      const msg = [];
      const wolf = pickUpUser(data, user.wolfId);
      data.forEach((item) => {
        let subtitle = '';
        const oldPlayers = previousData.filter(preItem => preItem.userId === item.userId);
        if (oldPlayers.length > 0) {
          if (wolf) {
            const isWolf = item.userId === wolf.userId;
            const scoreDiff = item.score - wolf.score;
            subtitle = isWolf ? '  🍗🍗🍗肥狼牛皮(破音)' : `（${nDiff(scoreDiff)}）`;
          }
          const old = oldPlayers[0];
          if (item.lastUpdateTime !== old.lastUpdateTime) {
            msg.push({
              newUser: false,
              subtitle,
              old,
              item,
            });
          }
        } else {
          if (wolf) {
            const scoreDiff = item.score - wolf.score;
            subtitle = `（${nDiff(scoreDiff)}）`;
          }
          msg.push({
            newUser: true,
            subtitle,
            item,
          });
        }
      });
      return msg;
    })
    .then((msg) => {
      if (firstBooted || msg.length === 0) {
        firstBooted = false;
        throw new Error('no change');
      }
      return msg;
    })
    .then((msg) => {
      sendDingtalkMessage(msg);
      sendWechatMessage(msg);
    })
    .catch((err) => {
      console.log(`${dateToString(new Date())}  |  ${err.message}`);
    });
});

http.createServer().listen(3000);
