const path = require('path');
const fs = require('fs');
const format = require('date-fns/format');

const DATE_STRING = 'YYYY-MM-DD_HH-mm-ss';

const dbPath = path.resolve(__dirname, './db.json');

const n = num => num.toFixed(5);

const nDiff = (num, digit = 5) => {
  if (typeof num !== 'number') return num;
  return `${num >= 0 ? '+' : '-'} ${Math.abs(num).toFixed(digit)}`;
};

const dateToString = date => format(date, DATE_STRING);

const readPreviousData = () => {
  if (!fs.existsSync(dbPath)) return [];
  // eslint-disable-next-line
  const data = fs.readFileSync(dbPath).toString()
  return JSON.parse(data);
};

const updateData = (data) => {
  if (fs.existsSync(dbPath)) {
    fs.copyFileSync(dbPath, path.join(__dirname, './history', `${dateToString(new Date())}---data.json`));
  }
  fs.writeFileSync(dbPath, JSON.stringify(data, null, 2));
  return data;
};

const pickUpUser = (data, id) => {
  const target = data.filter(item => item.userId === id);
  if (target.length > 0) return target[0];
  return null;
};


module.exports = {
  readPreviousData,
  updateData,
  n,
  nDiff,
  dateToString,
  pickUpUser,
};

