FROM liangzr/node:dubnium-alpine-git
WORKDIR /usr/src/app
RUN mkdir -p history
COPY package.json /usr/src/app
RUN yarn config set registry 'https://registry.npm.taobao.org' \
    && yarn install --prod
COPY . /usr/src/app
CMD ["node", "app.js"]